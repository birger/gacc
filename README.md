guest account interface
=======================

```
mkdir ~/.virtualenvs
python3 -m venv ~/.virtualenvs/gsoc
source ~/.virtualenvs/gsoc/bin/activate
pip install django
pip install django-ldapdb
```

you will have to change name, user and password in
project/settings.py to point to an actual ldap
server where the user has write access to the
base_dn specified below:
```
    'ldap': {
        'ENGINE': 'ldapdb.backends.ldap',
        'NAME': 'ldap://192.168.121.209/',
        'USER': 'cn=admin,dc=gsoc,dc=example,dc=org',
        'PASSWORD': 'password',
     },
```
also you will have to set the base_dn in accounts/models.py
```
    base_dn = "ou=people,dc=gsoc,dc=example,dc=org"
```

then run
```
python3 manage.py runserver
```

and access the webinterface on localhost:8000/login
