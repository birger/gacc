from django.urls import path, include
from . import views

urlpatterns = [
        path('', include('django.contrib.auth.urls')),
        path('signup/', views.signup, name='signup'),
        path('accounts/profile/', views.LdapUserDetail.as_view(), name='profile'),
]
