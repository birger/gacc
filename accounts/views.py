from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.views.generic.edit import UpdateView
from django.views.generic.detail import DetailView

from .models import LdapUser
from .forms import SignUpForm

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('signup')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})

class LdapUserDetail(DetailView):
    model = LdapUser

    def get_object(self):
        return LdapUser.objects.get(username=self.request.user)

class LdapUserUpdate(UpdateView):
    model = LdapUser
    fields = ['email', 'password']

    def get_object(self):
        """Return's the current users profile."""
        return LdapUser.objects.get(username=self.request.user)
